﻿using Bycomp.Common.Garp.Tables;
using Bycomp.LogFileWriter;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpDropShipOrder
{
    public class Functions : IDisposable
    {

        public void CreateOrders()
        {
            try
            {
                using (EtonDropShipmentEntities context = new EtonDropShipmentEntities())
                {
                    var orders = context.OrderHuvudImports.Where(s => s.status == 1).ToList();
                    foreach (var order in orders)
                    {
                        try
                        {
                            if (CheckOrderHead(order) && CheckAvailability(order))
                            {
                                CreateOrderHead(order);
                                AddOrderHeadText(order);
                                AddOrderHeadDeliveryAddress(order);
                                AddOrderRows(order);
                            }
                            else
                            {
                                order.cancelled = 1;
                            }
                            order.status = 2;
                        }
                        catch (Exception ex)
                        {
                            InserOrderHeadErrorText(order, ex.Message);
                            order.status = 9;
                            order.cancelled = 1;
                        }
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                wr.Write(ex.Message);
            }
        }

        private bool CheckOrderHead(OrderHuvudImport order)
        {
            if (!KA.Find(order.ERPKundnr)) throw new Exception($"Customer {order.ERPKundnr} does not exist");
            if (KA.KTY == "z") throw new Exception($"Customer {order.ERPKundnr} is frozen");
            if (!string.IsNullOrEmpty(KA.FKN))
            {
                if (!KA.Find(KA.FKN)) throw new Exception($"Invoice customer {KA.FKN} to customer {order.ERPKundnr} does not exist");
                if (KA.KTY == "z") throw new Exception($"INvoice customer {KA.KNR} to customer {order.ERPKundnr} is frozen");
            }
            return true;
        }

        private bool CheckAvailability(OrderHuvudImport order)
        {
            foreach (var row in order.OrderRadImports)
            {
                try
                {
                    var qty = GetAvailability(row.Artikelnr);
                    if (qty < row.Orderantal)
                    {
                        InsertOrderRowErrorText(row, $"No available quantiy. Order:{row.Orderantal} Stock:{qty}");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    InsertOrderRowErrorText(row, ex.Message);
                    throw ex;
                }
            }
            return true; ;
        }

        private static void InsertOrderRowErrorText(OrderRadImport row, string text)
        {
            row.OrderRadImportErrors.Add(new OrderRadImportError()
            {
                fk_orderradimport_id = row.pk_orderradimport_id,
                Text = text
            });
        }

        private static void InserOrderHeadErrorText(OrderHuvudImport head, string text)
        {
            head.OrderHuvudImportErrors.Add(new OrderHuvudImportError()
            {
                fk_orderhuvudimport_id = head.pk_orderhuvudimport_id,
                Text = text
            });
        }

        private decimal GetAvailability(string anr)
        {
            if (!AGA.Find(anr)) throw new Exception($"Item {anr} does not exist");
            if (AGA.TYP == "z") throw new Exception($"Item {anr} is frozen");
            return decimal.Parse(AGA.LGA, ci) - decimal.Parse(AGA.ORA, ci) - decimal.Parse(AGA.ROA, ci);
        }

        private void AddOrderRows(OrderHuvudImport order)
        {
            byte rdc = 0;
            if (!OGR.Find(order.ERPOrdernr.PadRight(6) + "250")) OGR.Prior();
            if (!OGR.Bof && OGR.ONR.TrimEnd() == order.ERPOrdernr) rdc = byte.Parse(OGR.RDC);
            foreach (var row in order.OrderRadImports)
            {
                OGR.Insert();
                OGR.ONR = order.ERPOrdernr;
                OGR.RDC = (++rdc).ToString();
                OGR.ANR = row.Artikelnr;
                OGR.ORA = row.Orderantal.ToString(ci);
                OGR.PRI = row.Pris.ToString(ci);
                OGR.LAG = conf.GarpStockId;
                OGR.Post();
                row.ERPRadnr = OGR.RDC;
            }
        }

        private void AddOrderHeadDeliveryAddress(OrderHuvudImport order)
        {
            if (order.OrderLevAdressImports != null)
            {
                var ola = order.OrderLevAdressImports.FirstOrDefault();
                if (ola != null)
                {
                    if (!OGL.Find(order.ERPOrdernr))
                    {
                        OGL.Insert();
                        OGL.ONR = order.ERPOrdernr;
                    }
                    OGL.NAM = ola.Namn;
                    OGL.AD1 = ola.Adress1;
                    OGL.AD2 = ola.Adress2;
                    OGL.ORT = $"{ola.Postnr.Replace(" ", "")}  {ola.Ort}".PadRight(30).Substring(0, 27) + " " + (ola.Region ?? "").PadRight(2).Substring(0, 2);
                    OGL.LND = ola.Land;
                    OGL.Post();

                    if (!OGK.Find(order.ERPOrdernr.PadRight(6) + "  0255")) OGK.Prior();
                    byte sqc = 0;
                    if (!OGK.Bof && OGK.ONR.TrimEnd() == order.ERPOrdernr.TrimEnd()) sqc = byte.Parse(OGK.RDC);
                    if (sqc > 246) throw new Exception($"Not enough textrows available for saving address on orded {order.ERPOrdernr}");
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_NAM:", ola.Namn);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_AD1:", ola.Adress1);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_AD2:", ola.Adress2);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_PNR:", ola.Postnr);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_ORT:", ola.Ort);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_REG:", ola.Region);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_LND:", ola.Land);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_TEL:", ola.Telefon);
                    AddAddressText(order.ERPOrdernr, ++sqc, "ADR_IMA:", ola.Mail);
                }
            }
        }

        private void AddAddressText(string onr, byte sqc, string typ, string txt)
        {
            OGK.Insert();
            OGK.ONR = onr;
            OGK.RDC = "0";
            OGK.SQC = sqc.ToString();
            OGK.OBF = "0";
            OGK.PLF = "0";
            OGK.FSF = "0";
            OGK.FAF = "0";
            OGK.TX1 = $"{typ} {txt}";
            OGK.Post();
        }

        private void AddOrderHeadText(OrderHuvudImport order)
        {
            if (!string.IsNullOrEmpty(order.Inkopsordernr))
            {
                if (!OGF.Find(order.ERPOrdernr))
                {
                    OGF.Insert();
                    OGF.ONR = order.ERPOrdernr;
                    OGF.TX1 = order.Inkopsordernr;
                    OGF.Post();
                }
            }
        }

        private void CreateOrderHead(OrderHuvudImport order)
        {
            ga.Counters.Series = conf.GarpOrderSerie;
            for (int i = 0; i < 10; i++)
            {
                var onr = ga.Counters.GetCustomerOrderNo(true);
                if (!OGA.Find(onr))
                {
                    OGA.Insert();
                    OGA.ONR = onr;
                    OGA.KNR = order.ERPKundnr;
                    OGA.LSE = order.ERPLevsatt;
                    OGA.Post();
                    order.ERPOrdernr = onr;
                    return;
                }
            }
            throw new Exception($"No more available order number in orderserie '{conf.GarpOrderSerie}'");
        }

        #region Properties

        CultureInfo ci => CultureInfo.GetCultureInfo("en-US");

        Configuration _conf;
        Configuration conf => _conf ?? (_conf = new Configuration());

        Writer _wr;
        Writer wr => _wr ?? (_wr = new Writer(conf.LogFolder, "Bycomp_", ".log", true));

        Garp.Application _ga;
        Garp.Application ga
        {
            get
            {
                if (_ga == null)
                {
                    _ga = new Garp.Application();
                    if (!_ga.Login(conf.GarpUID, conf.GarpPWD)) throw new Exception("Could not login to Garp with specified user and password");
                    if (_ga.Bolag != conf.GarpBOL) _ga.SetBolag(conf.GarpBOL);
                    if (_ga.Bolag != conf.GarpBOL) throw new Exception($"Could not change to company '{conf.GarpBOL}'");
                }
                return _ga;
            }
        }


        #endregion

        #region Tables

        tblKA _ka;
        tblKA KA => _ka ?? (_ka = new tblKA(ga));

        tblOGA _oga;
        tblOGA OGA => _oga ?? (_oga = new tblOGA(ga));

        tblOGF _ogf;
        tblOGF OGF => _ogf ?? (_ogf = new tblOGF(ga));

        tblOGL _ogl;
        tblOGL OGL => _ogl ?? (_ogl = new tblOGL(ga));

        tblOGR _ogr;
        tblOGR OGR => _ogr ?? (_ogr = new tblOGR(ga));

        tblOGK _ogk;
        tblOGK OGK => _ogk ?? (_ogk = new tblOGK(ga));

        tblAGA _aga;
        tblAGA AGA => _aga ?? (_aga = new tblAGA(ga));

        tblAGT _agt;
        tblAGT AGT => _agt ?? (_agt = new tblAGT(ga));

        #endregion

        #region Dispose

        bool disposed = false;

        ~Functions()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                try
                {
                    if (ga != null)
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(ga);
                    }
                }
                finally
                {
                    this.disposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion

    }
}

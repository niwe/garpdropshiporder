﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpDropShipOrder
{
    public class Configuration
    {
        NameValueCollection appSettings = ConfigurationManager.AppSettings;

        public string GarpUID => GetSetting("GarpUID");

        public string GarpPWD => GetSetting("GarpPWD");

        public string GarpBOL => GetSetting("GarpBOL");

        public string GarpOrderSerie => GetSetting("GarpOrderSerie");

        public string GarpStockId => GetSetting("GarpStockId");

        public string LogFolder => GetSetting("LogFolder").TrimEnd('\\');

        public bool SendErrorMail => bool.Parse(GetSetting("SendErrorMail"));

        public string SmtpServer => GetSetting("SmtpServer");

        public int SmtpPort => int.Parse(GetSetting("SmtpPort"));

        public string SmtpUser => GetSetting("SmtpUser");

        public string SmtpPassword => GetSetting("SmtpPassword");

        public string SmtpFrom => GetSetting("SmtpFrom");

        public bool SmtpUseSSL => bool.Parse(GetSetting("SmtpUseSSL"));

        public bool SmtpUseCredentials => bool.Parse(GetSetting("SmtpUseCredentials"));

        public string SmtpSubject => GetSetting("SmtpSubject");

        public string SmtpReceiverAddress => GetSetting("SmtpReceiverAddress");

        public string SmtpReceiverName => GetSetting("SmtpReceiverName");


        private string GetSetting(string key)
        {
            var tmp = appSettings.Get(key);
            if (tmp == null) throw new Exception(string.Format("Setting key: {0}, could not be found", key));
            return tmp;
        }

        private List<string> GetSettings(string key)
        {
            var tmp = appSettings.Get(key);
            if (tmp == null) throw new Exception(string.Format("Setting key: {0}, could not be found", key));
            return tmp.Split(';').ToList();
        }
    }
}
